import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

import {User} from '../_models/user';
import {Role} from '../_models/role';
import {UserService} from '../_services/user.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  userId: number;
  user: User;
  allRoles: { role: Role, selected: boolean }[] = [];
  error = '';
  loading = false;

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.userId = this.route.snapshot.params['userId'];
    this.getUserData();
  }

  save(): void {
    this.userService.update(this.user)
      .then(() => this.location.back());
  }

  getUserData(): void {
    Promise.all([
      this.userService.getRoles(),
      this.userService.getUser(this.userId)
    ]).then(
      result => {
        const allRoles = result[0];
        const user = result[1];
        const userRoles = user.roles;
        allRoles.forEach(role => {
          if (userRoles.find(userRole => userRole.id === role.id)) {
            this.allRoles.push({role, selected: true});
          } else {
            this.allRoles.push({role, selected: false});
          }
        });
        this.user = user;
      });
  }

  selectedRoles(): void {
    this.user.roles = this.allRoles.filter(role => role.selected === true).map(
      role => {
        return role.role;
      }
    );
  }
}
