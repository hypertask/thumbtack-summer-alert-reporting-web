import {Advertiser} from './advertiser';
import {CampaignStatus} from './campaign-status';
import {Creative} from './creative';

export class Campaign {
  id: number;
  advertiser: Advertiser;
  status: CampaignStatus;
  name: string;
  description: string;
  startDate: string;
  endDate: string;
  impressionCap: number;
  budgetCap: number;
  creatives: Creative[];
}
