import {Campaign} from './campaign';
import {CreativeStatus} from './creative-status';
import {CreativeType} from './creative-type';
import {CreativeDimension} from './creative-dimension';

export class Creative {
  id: number;
  campaign: Campaign;
  name: string;
  description: string;
  landingPage: string;
  price: number;
  status: CreativeStatus;
  type: CreativeType;
  dimensions: CreativeDimension;
}
