export class CreativeDimension {
  id: number;
  width: number;
  height: number;
}
