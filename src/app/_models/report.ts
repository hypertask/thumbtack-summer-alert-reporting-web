import {Issue} from './issue';

export class Report {
  id: number;
  campaignId: number;
  campaignName: string;
  created: string;
  issues: Issue[];
}
