import {Campaign} from './campaign';

export class Advertiser {
  id: number;
  name: string;
  description: string;
  site: string;
  campaigns: Campaign[];
}
