import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Report} from '../_models/report';
import {AuthService} from './auth.service';
import {Campaign} from '../_models/campaign';

@Injectable()
export class CampaignService {
  private headers = new Headers(
    {
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    });
  private campaignsUrl = 'http://localhost:8080/campaigns';

  constructor(private http: Http, private authService: AuthService) {
  }

  getFreeCampaigns(): Promise<Campaign[]> {
    const url = `${this.campaignsUrl}/free`;
    this.updateHeaders();
    return this.http
      .get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as Report[])
      .catch(this.handleError);
  }

  private updateHeaders() {
    this.headers.delete('Authorization');
    this.headers.append('Authorization', this.authService.getToken());
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
