import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Report} from '../_models/report';
import {AuthService} from './auth.service';
import {Rule} from '../_models/rule';

@Injectable()
export class ReportService {
  private headers = new Headers(
    {
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    });
  private reportsUrl = 'http://localhost:8080/reports';

  constructor(private http: Http, private authService: AuthService) {
  }

  getReports(): Promise<Report[]> {
    this.updateHeaders();
    return this.http
      .get(this.reportsUrl, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as Report[])
      .catch(this.handleError);
  }

  validate(rules: string[]): Promise<Report[]> {
    this.updateHeaders();
    return this.http
      .post(this.reportsUrl, JSON.stringify(rules), {headers: this.headers})
      .toPromise()
      .then(response => response.json() as Report[])
      .catch(this.handleError);
  }

  getRuleCodes(): Promise<Rule[]> {
    const url = `${this.reportsUrl}/rules`;
    this.updateHeaders();
    return this.http
      .get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as Rule[])
      .catch(this.handleError);
  }

  private updateHeaders() {
    this.headers.delete('Authorization');
    this.headers.append('Authorization', this.authService.getToken());
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
