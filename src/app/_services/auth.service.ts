import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private authUrl = 'http://localhost:8080/login';

  constructor(private http: Http) {
  }

  login(username: string, password: string): Observable<boolean> {
    return this.http
      .post(this.authUrl, JSON.stringify({
        username: username,
        password: password
      }), {headers: this.headers})
      .map((response: Response) => {
        const token = response.text();
        if (token) {
          localStorage.setItem('currentUser', JSON.stringify({username: username, token: token}));
          return true;
        } else {
          return false;
        }
      }).catch((error: any) => Observable.throw(error || 'Server error'));
  }

  isLoggedIn(): boolean {
    const token: String = this.getToken();
    return token && token.length > 0;
  }

  getToken(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser && currentUser.token;
    return token ? token : '';
  }

  logout(): void {
    localStorage.removeItem('currentUser');
  }
}
