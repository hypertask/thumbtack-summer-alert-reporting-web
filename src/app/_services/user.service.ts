import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {User} from '../_models/user';
import {AuthService} from './auth.service';
import {Campaign} from '../_models/campaign';
import {Role} from '../_models/role';

@Injectable()
export class UserService {
  private headers = new Headers(
    {
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    });
  private usersUrl = 'http://localhost:8080/users';

  constructor(private http: Http, private authService: AuthService) {
  }

  getUsers(): Promise<User[]> {
    this.updateHeaders();
    return this.http
      .get(this.usersUrl, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as User[])
      .catch(this.handleError);
  }

  getUser(id: number): Promise<User> {
    this.updateHeaders();
    const url = `${this.usersUrl}/${id}`;
    return this.http
      .get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }

  getUsersCampaign(id: number): Promise<Campaign[]> {
    this.updateHeaders();
    const url = `${this.usersUrl}/${id}/campaigns`;
    return this.http
      .get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as Campaign[]);
  }

  assign(userId: number, campaignId: number): Promise<void> {
    this.updateHeaders();
    const url = `${this.usersUrl}/${userId}/campaigns/${campaignId}`;
    return this.http
      .put(url, null, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  unassign(userId: number, campaignId: number): Promise<void> {
    this.updateHeaders();
    const url = `${this.usersUrl}/${userId}/campaigns/${campaignId}`;
    return this.http
      .delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  update(user: User): Promise<void> {
    this.updateHeaders();
    const url = `${this.usersUrl}/${user.id}`;
    return this.http
      .put(url, JSON.stringify(user), {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  delete(userId: number): Promise<void> {
    this.updateHeaders();
    const url = `${this.usersUrl}/${userId}`;
    return this.http
      .delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  getRoles(): Promise<Role[]> {
    this.updateHeaders();
    const url = `${this.usersUrl}/roles`;
    return this.http
      .get(url, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as Role[]);
  }

  add(user: User) {
    this.updateHeaders();
    return this.http
      .post(this.usersUrl, JSON.stringify(user), {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private updateHeaders() {
    this.headers.delete('Authorization');
    this.headers.append('Authorization', this.authService.getToken());
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
