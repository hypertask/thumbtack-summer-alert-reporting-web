import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Location} from '@angular/common';
import {UserService} from '../_services/user.service';

import 'rxjs/add/operator/switchMap';
import {Campaign} from '../_models/campaign';
import {CampaignService} from '../_services/campaign.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-campaigns.component.html',
  styleUrls: ['./user-campaigns.component.css']
})
export class UserCampaignsComponent implements OnInit {
  userId: number;
  campaigns: Campaign[];
  freeCampaigns: Campaign[];
  selectedFreeCampaign: Campaign;
  error = '';
  loading = false;

  constructor(private userService: UserService, private campaignService: CampaignService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.userId = this.route.snapshot.params['userId'];
    this.getUserCampaigns();
    this.getFreeCampaigns();
  }

  getUserCampaigns(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.userService.getUsersCampaign(+params.get('userId')))
      .subscribe(campaigns => this.campaigns = campaigns);
  }

  getFreeCampaigns(): void {
    this.campaignService.getFreeCampaigns()
      .then(freeCampaigns => this.freeCampaigns = freeCampaigns);
  }

  assign(campaignId: number): void {
    this.userService.assign(this.userId, campaignId)
      .then(
        () => {
          this.freeCampaigns = this.freeCampaigns.filter(campaign => campaign.id !== campaignId);
          this.getUserCampaigns();
        });
  }

  unassign(campaignId: number): void {
    this.userService.unassign(this.userId, campaignId)
      .then(() => {
        this.campaigns = this.campaigns.filter(campaign => campaign.id !== campaignId);
        this.getFreeCampaigns();
      });
  }
}
