import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ReportsComponent} from './reports/reports.component';
import {UsersComponent} from './users/users.component';
import {UserDetailComponent} from './user/user-detail.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './_guards/auth.guard';
import {LogoutComponent} from './logout/logout.component';
import {UserCampaignsComponent} from './campaigns/user-campaigns.component';
import {NewUserComponent} from './new-user/new-user.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent, canActivate: [AuthGuard]},
  {path: 'reports', component: ReportsComponent, canActivate: [AuthGuard]},
  {path: 'users/add', component: NewUserComponent, canActivate: [AuthGuard]},
  {path: 'users/:userId', component: UserDetailComponent, canActivate: [AuthGuard]},
  {path: 'users/:userId/campaigns', component: UserCampaignsComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
