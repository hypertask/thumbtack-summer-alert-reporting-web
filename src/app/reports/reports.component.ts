import {Component, OnInit} from '@angular/core';
import {Report} from '../_models/report';
import {ReportService} from '../_services/report.service';
import {Rule} from '../_models/rule';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
  providers: []
})
export class ReportsComponent implements OnInit {

  reports: Report[];
  rules: Rule[];
  selectedRules: string[];

  constructor(private reportService: ReportService) {
  }

  ngOnInit(): void {
    this.getRuleCodes();
    this.getReports();
  }

  validate(rules: string[]): void {
    this.reportService.validate(rules).then(reports => this.reports = this.reports.concat(reports));
  }

  getReports(): void {
    this.reportService.getReports().then(reports => this.reports = reports);
  }

  getRuleCodes(): void {
    this.reportService.getRuleCodes().then(ruleCodes => this.rules = ruleCodes);
    console.log(this.rules);
  }
}
