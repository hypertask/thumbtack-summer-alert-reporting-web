import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';

import {User} from '../_models/user';
import {Role} from '../_models/role';
import {UserService} from '../_services/user.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-add-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  user: User = new User;
  allRoles: { role: Role, selected: boolean }[] = [];
  error = '';
  loading = false;

  constructor(private userService: UserService,
              private location: Location) {
  }

  ngOnInit(): void {
    this.getRoles();
    console.log(this.allRoles);
  }

  save(): void {
    this.userService.add(this.user)
      .then(() => this.location.back());
  }

  getRoles(): void {
    this.userService.getRoles().then(roles => roles.forEach(
      role => this.allRoles.push({role, selected: false})
    ));
  }

  selectedRoles(): void {
    this.user.roles = this.allRoles.filter(role => role.selected === true).map(
      role => {
        return role.role;
      }
    );
  }
}
