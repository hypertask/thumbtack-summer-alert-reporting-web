import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {UserDetailComponent} from './user/user-detail.component';
import {UserService} from './_services/user.service';
import {UsersComponent} from './users/users.component';
import {ReportsComponent} from './reports/reports.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {AuthService} from './_services/auth.service';
import {AuthGuard} from './_guards/auth.guard';
import {ReportService} from './_services/report.service';
import {LogoutComponent} from './logout/logout.component';
import {UserCampaignsComponent} from './campaigns/user-campaigns.component';
import {CampaignService} from './_services/campaign.service';
import {NewUserComponent} from './new-user/new-user.component';

@NgModule({
  declarations: [
    AppComponent,
    UserDetailComponent,
    UsersComponent,
    ReportsComponent,
    HomeComponent,
    LoginComponent,
    LogoutComponent,
    UserCampaignsComponent,
    NewUserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [
    UserService,
    AuthService,
    ReportService,
    CampaignService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
