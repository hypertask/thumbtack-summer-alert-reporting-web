import { ThumbtackSummerAlertReportingWebPage } from './app.po';

describe('thumbtack-summer-alert-reporting-web App', () => {
  let page: ThumbtackSummerAlertReportingWebPage;

  beforeEach(() => {
    page = new ThumbtackSummerAlertReportingWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
